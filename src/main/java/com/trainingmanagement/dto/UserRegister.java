package com.trainingmanagement.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRegister {
    @NotBlank
    @Email
    protected String username;

    @NotBlank
    protected String password;

    protected String firstName;
    protected String lastName;
}
