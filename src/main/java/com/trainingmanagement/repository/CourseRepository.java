package com.trainingmanagement.repository;

import com.trainingmanagement.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {
    /*List<Course> findFirstByDateHoursIsOrderByDateDateDesc(int date_hours);*/

    @Query(" from Course cs where (:query is null or (lower(cs.title) like concat('%', lower(:query), '%')) or (lower(cs.description) like concat('%', lower(:query), '%')))")
    List<Course> getListByQuery(String query);

    Optional<Course> getCourseByTitleIgnoreCase(String title);
}
