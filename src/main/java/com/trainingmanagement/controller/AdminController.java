package com.trainingmanagement.controller;

import com.trainingmanagement.dto.UserRegister;
import com.trainingmanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController {

    private final UserService userService;

    @PostMapping("/create-leader")
    public ResponseEntity<?> registerLeader(@RequestBody UserRegister leader) {
        return userService.registLeader(leader);
    }

    @DeleteMapping("/delete-leader")
    public ResponseEntity<?> deleteLeader(@PathVariable String leaderId) {
         boolean deleted = userService.deleteLeader(leaderId);
         if(deleted) {
             return new ResponseEntity<>("Leader deleted successfully", HttpStatus.ACCEPTED);
         } else {
             return new ResponseEntity<>("Failed to delete leader user", HttpStatus.BAD_REQUEST);
         }
    }
}
