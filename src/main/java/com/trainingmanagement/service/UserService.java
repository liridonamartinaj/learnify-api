package com.trainingmanagement.service;

import com.trainingmanagement.dto.ServiceResponse;
import com.trainingmanagement.dto.UserRegister;
import com.trainingmanagement.entity.User;
import com.trainingmanagement.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public ResponseEntity<?> createAdmin(UserRegister admin) {
        admin.setFirstName(admin.getFirstName());
        admin.setLastName(admin.getLastName());
        admin.setUsername(admin.getUsername());
        admin.setPassword(admin.getPassword());
        return ResponseEntity.ok(ServiceResponse.ok(admin));
    }

    public ResponseEntity<?> registLeader(UserRegister dto) {
        return userRepository.findUserByUsername(dto.getUsername())
                .map(o -> ResponseEntity.ok(ServiceResponse.error("Leader is already registered")))
                .orElseGet(() -> {
                    User newLeader = new User();
                    /*newLeader.setUserType(Role.LEADER);
                    newLeader.setUserStatus(UserStatus.ACTIVE);*/
                    newLeader.setUsername(dto.getUsername());
                    newLeader.setPassword(passwordEncoder.encode(dto.getPassword()));
                    newLeader.setFirstName(dto.getUsername());
                    newLeader.setLastName(dto.getLastName());
                    userRepository.save(newLeader);
                    return ResponseEntity.ok(ServiceResponse.ok(newLeader));
                });
    }

    public boolean deleteLeader(String leaderId) {
        User existingLecturer = userRepository.findUserByUsername(leaderId).orElse(null);
       /* if(existingLecturer != null && existingLecturer.getUserType() == Role.LEADER) {
            userRepository.delete(existingLecturer);
            return true;
        }*/
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findUserByUsername(username)
                .map(user -> {
                    Set<GrantedAuthority> authorities = Stream.of(user.getRole()
                                    .split(","))
                            .map(o -> new SimpleGrantedAuthority("ROLE_" + o))
                            .collect(Collectors.toSet());

                    return new org.springframework.security.core.userdetails
                            .User(user.getUsername(), user.getPassword(), authorities);
                })
                .orElseThrow(() -> new UsernameNotFoundException("Username not found: " + username));
    }
}
