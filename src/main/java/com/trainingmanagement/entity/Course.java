package com.trainingmanagement.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Table(name = "tm_course")
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class Course extends BaseEntity{

    @Column(name = "title")
    private String title;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "image")
    private String image;

    @Column(name = "price")
    private Double price;

    @Column(name = "sale")
    private Boolean sale = false;

    @Column(name = "sale_price")
    private Double salePrice;

    @Column(name = "featured")
    private Boolean featured;

    @Column(name = "category_id")
    private Long categoryId;

    @ManyToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    private Category category;
}
