package com.trainingmanagement.configuration;

import com.trainingmanagement.dto.ServiceResponse;
import com.trainingmanagement.dto.UserRegister;
import com.trainingmanagement.service.CourseService;
import com.trainingmanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class StartUpRunner  implements CommandLineRunner {

    private final UserService userService;
    private final CourseService courseService;


    @Override
    public void run(String... args) throws Exception {
        var result = userService.createAdmin(UserRegister.builder()
                .firstName("Admin")
                .lastName("Admin")
                .password("admin")
                .username("admin@gmail.com")
                .build());
        log.error("Admin user created: {}", ((ServiceResponse<?>)result.getBody()).isSuccess());

        courseService.createInitialCourses();
    }
}

