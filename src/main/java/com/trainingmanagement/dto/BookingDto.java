package com.trainingmanagement.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingDto {
    @NotBlank
    protected String firstName;
    @NotBlank
    protected String lastName;
    @NotBlank
    @Email
    protected String email;
    @NotBlank
    protected String phoneNumber;
    @NotEmpty
    protected Long courseId;
}
