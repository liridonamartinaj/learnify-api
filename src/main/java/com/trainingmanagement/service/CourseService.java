package com.trainingmanagement.service;

import com.trainingmanagement.dto.BookingDto;
import com.trainingmanagement.entity.Booking;
import com.trainingmanagement.entity.Category;
import com.trainingmanagement.entity.Course;
import com.trainingmanagement.repository.BookingRepository;
import com.trainingmanagement.repository.CategoryRepository;
import com.trainingmanagement.repository.CourseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Slf4j
@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;
    private final CategoryRepository categoryRepository;
    private final BookingRepository bookingRepository;

    public List<Course> getCourses(int dateHours) {
        return courseRepository.findAll();
    }

    public List<Course> getList(String query) {
        return courseRepository.getListByQuery(query);
    }

    public void createInitialCourses() {

        Category firstCategory = categoryRepository.findCategoryByTitleIgnoreCase("Frontend")
                                .orElseGet(() -> {
                                    Category item = new Category();
                                    item.setTitle("Frontend");
                                    return categoryRepository.save(item);
                                });

        Category secondCategory = categoryRepository.findCategoryByTitleIgnoreCase("Backend")
                .orElseGet(() -> {
                    Category item = new Category();
                    item.setTitle("Backend");
                    return categoryRepository.save(item);
                });

        Category thirdCategory = categoryRepository.findCategoryByTitleIgnoreCase("Database")
                .orElseGet(() -> {
                    Category item = new Category();
                    item.setTitle("Database");
                    return categoryRepository.save(item);
                });

        courseRepository.getCourseByTitleIgnoreCase("Java from scratch")
                .ifPresentOrElse(o -> log.info("Java from scratch exists"), () -> {
                    Course item = Course.builder()
                            .title("Java from scratch")
                            .description(
                                    """
                                    Java From Scratch (Java Tutorial) is a comprehensive guide that aims 
                                    to provide beginners with a solid foundation in Java programming. 
                                    Whether you're new to programming or have some experience in other 
                                    languages, this tutorial will take you through the basics of Java and 
                                    gradually introduce more advanced topics.
                                    """
                            )
                            .featured(true)
                            .image("https://www.jrebel.com/sites/default/files/image/2020-05/image-blog-revel-top-java-tools.jpg")
                            .sale(false)
                            .price(1000d)
                            .categoryId(secondCategory.getId())
                            .build();
                    courseRepository.save(item);
                });


        courseRepository.getCourseByTitleIgnoreCase("Python")
                .ifPresentOrElse(o -> log.info("Python  exists"), () -> {
                    Course item = Course.builder()
                            .title("Python ")
                            .description(
                                    """
                                    Python is a high-level, general-purpose programming language.
                                    Its design philosophy emphasizes code readability with the use of significant indentation.
                                    Python is dynamically typed and garbage-collected. It supports multiple programming paradigms, 
                                    including structured, object-oriented and functional programming.
                                    """
                            )
                            .featured(true)
                            .sale(false)
                            .image("https://cdn.activestate.com/wp-content/uploads/2021/12/python-coding-mistakes.jpg")
                            .price(1000d)
                            .categoryId(secondCategory.getId())
                            .build();
                    courseRepository.save(item);
                });

        courseRepository.getCourseByTitleIgnoreCase("Angular ")
                .ifPresentOrElse(o -> log.info("Angular exists"), () -> {
                    Course item = Course.builder()
                            .title("Angular ")
                            .description(
                                    """
                                    Angular is a TypeScript-based, free and open-source single-page web application
                                    framework led by the Angular Team at Google and by a community of individuals and 
                                    corporations. Angular is a complete rewrite from the same team that built AngularJS.     
                                    """

                            )
                            .featured(true)
                            .sale(false)
                            .image("https://miro.medium.com/v2/resize:fit:1400/1*bMgQ8MhbnQexpqHgIgBJPA.png")
                            .price(1000d)
                            .categoryId(firstCategory.getId())
                            .build();
                    courseRepository.save(item);
                });

        courseRepository.getCourseByTitleIgnoreCase("Javascript")
                .ifPresentOrElse(o -> log.info("Javascript exists"), () -> {
                    Course item = Course.builder()
                            .title("Javascript")
                            .description(
                                    """
                                    JavaScript, often abbreviated as JS, is a programming language that 
                                    is one of the core technologies of the World Wide Web, alongside HTML and CSS.
                                    As of 2023, 98.7% of websites use JavaScript on the client side for webpage behavior, 
                                    often incorporating third-party libraries.                                    
                                    """
                            )
                            .featured(false)
                            .sale(true)
                            .image("https://miro.medium.com/v2/resize:fit:1200/1*LyZcwuLWv2FArOumCxobpA.png")
                            .price(1000d)
                            .salePrice(500d)
                            .categoryId(firstCategory.getId())
                            .build();
                    courseRepository.save(item);
                });


        courseRepository.getCourseByTitleIgnoreCase("MySql ")
                .ifPresentOrElse(o -> log.info("MySql exists"), () -> {
                    Course item = Course.builder()
                            .title("MySql")
                            .description(
                                    """      
                                    MySQL is an open-source relational database management system. Its name
                                     is a combination of "My", the name of co-founder Michael Widenius's daughter My, 
                                     and "SQL", the acronym for Structured Query Language
                                    """
                            )
                            .featured(false)
                            .sale(false)
                            .image("https://www.zend.com/sites/default/files/image/2020-04/image-blog-mysql-php.jpg")
                            .price(1000d)
                            .categoryId(thirdCategory.getId())
                            .build();
                    courseRepository.save(item);
                });

        courseRepository.getCourseByTitleIgnoreCase("Oracle ")
                .ifPresentOrElse(o -> log.info("Oracle exists"), () -> {
                    Course item = Course.builder()
                            .title("Oracle")
                            .description(
                                    """
                                    Oracle Database is a proprietary multi-model database management system produced 
                                    and marketed by Oracle Corporation. It is a database commonly used for running online 
                                    transaction processing, data warehousing and mixed database workloads.
                                    """
                            )
                            .image("https://www.purestorage.com/content/dam/purestorage/knowledge/oracle-database.jpg.imgo.jpg")
                            .featured(true)
                            .sale(false)
                            .price(1500d)
                            .categoryId(thirdCategory.getId())
                            .build();
                    courseRepository.save(item);
                });

        courseRepository.getCourseByTitleIgnoreCase("ReactJS")
                .ifPresentOrElse(o -> log.info("ReactJS exist"), () -> {
                    Course item = Course.builder()
                            .title("ReactJS")
                            .description(
                                    """
                                    React is a free and open-source front-end JavaScript library for 
                                    building user interfaces based on components. It is maintained by Meta
                                    and a community of individual developers and companies. React can be used to 
                                    develop single-page, mobile, or server-rendered applications with frameworks like Next.js.
                                    """
                            )
                            .image("https://res.cloudinary.com/practicaldev/image/fetch/s--VwK_F8Np--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/o9r97ts6fecb6elzm4vf.png")
                            .featured(true)
                            .sale(false)
                            .price(1000d)
                            .categoryId(firstCategory.getId())
                            .build();
                    courseRepository.save(item);
                });


               courseRepository.getCourseByTitleIgnoreCase("C#")
                       .ifPresentOrElse(o -> log.info("C# exist"), () -> {
                           Course item = Course.builder()
                                   .title("C#")
                                   .description(
                                           """                                      
                                           C# is a general-purpose high-level programming language supporting 
                                           multiple paradigms. C# encompasses static typing, strong typing, lexically
                                           scoped, imperative, declarative, functional, generic, object-oriented, and 
                                           component-oriented programming disciplines.
                                           """
                                   )
                                   .image("https://bs-uploads.toptal.io/blackfish-uploads/components/blog_post_page/content/cover_image_file/cover_image/907886/retina_1708x683_cover-0219-The10MistakesC-Waldek_img-343f93efd75d26a7d857e374cd8630fd.png")
                                   .featured(true)
                                   .sale(false)
                                   .price(1000d)
                                   .categoryId(secondCategory.getId())
                                   .build();
                           courseRepository.save(item);
                       });




                courseRepository.getCourseByTitleIgnoreCase("R")
                          .ifPresentOrElse(o -> log.info("R exist"), () -> {
                              Course item = Course.builder()
                                      .title("R")
                                      .description(
                                              """   
                                              R is a programming language for statistical computing and graphics 
                                              supported by the R Core Team and the R Foundation for Statistical Computing.                                          
                                              """
                                      )
                                      .image("https://images.idgesg.net/images/article/2019/02/high_voltage_letter_r_programming_language_by_vrender_gettyimages_plus_circles_halftone_by_hakkiarslan_gettyimages_2400x1600-100789395-large.jpg?auto=webp&quality=85,70")
                                      .featured(true)
                                      .sale(false)
                                      .price(1000d)
                                      .categoryId(secondCategory.getId())
                                      .build();
                              courseRepository.save(item);
                          });



    }

    public Course getById(Long id) {
        return courseRepository.findById(id).orElseGet(() -> null);
    }

    public String book(BookingDto dto) {
        var dbCourse = courseRepository.findById(dto.getCourseId());
        if (dbCourse.isPresent()) {
            Booking booking = new Booking();
            booking.setFirstName(dto.getFirstName());
            booking.setLastName(dto.getFirstName());
            booking.setCourseId(dto.getCourseId());
            booking.setEmail(dto.getEmail());
            booking.setPhoneNumber(dto.getPhoneNumber());
            bookingRepository.save(booking);
            return "Booking was successfully completed. You will be contacted very soon!";
        }
        else {
            return "Course not found in database";
        }
    }
}
