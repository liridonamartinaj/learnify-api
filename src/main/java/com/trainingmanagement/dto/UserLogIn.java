package com.trainingmanagement.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserLogIn {

    @NotBlank
    @Email
    protected String username;
    @NotBlank
    protected String password;
}
