package com.trainingmanagement.controller;

import com.trainingmanagement.dto.BookingDto;
import com.trainingmanagement.entity.Course;
import com.trainingmanagement.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @GetMapping("/byDateHours/{dateHours}")
    public List<Course> getCoursesByDateHours(@PathVariable int dateHours) {
        return courseService.getCourses(dateHours);
    }

    @GetMapping("list")
    public List<Course> getList(@RequestParam("query") String query) {
        return courseService.getList(query);
    }

    @GetMapping("detail")
    public Course detail(@RequestParam("id") Long id) {
        return courseService.getById(id);
    }

    @PostMapping("book")
    public Object book(@RequestBody BookingDto dto) {
        String response = courseService.book(dto);
        return new Object() {
            public String message = response;
        };
    }
}
