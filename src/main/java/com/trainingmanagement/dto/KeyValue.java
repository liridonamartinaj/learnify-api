package com.trainingmanagement.dto;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KeyValue <K, V>{
    protected K key;
    protected V value;
}
