package com.trainingmanagement.repository;

import com.trainingmanagement.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    Optional<Category> findCategoryByTitleIgnoreCase(String title);
}
